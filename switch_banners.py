"""This script is used to update login banners on Dozart switches."""

#!/usr/bin/env python3

from netmiko import ConnectHandler
from colorama import Fore, Style
import configparser

# Read inventory file
config = configparser.ConfigParser()
config.read('inventory.ini')

print('Starting script.\n')

log_file = open('switch_banners.log', 'w')
print('Results of switch banners being updated:\n\n', file=log_file)

# Initialize switch list
switch_list = []

# Iterate through switches in the configuration file
for switch_name, switch_ip in config.items('switches'):
    # Netmiko parameters used to connect to switch
    exos_switch = {
        'device_type': 'extreme_exos',
        'host': switch_ip,
        'username': 'admin',
        'password': '',
    }
    switch_list.append(exos_switch)

    # Start connection to switch
    connection = ConnectHandler(**exos_switch)
    print(Fore.YELLOW + f'Connection started to {switch_name} ({switch_ip})' + Style.RESET_ALL)

    # File to execute commands
    config_file = 'switch_banners.conf'

    # Output commands from file to update banners
    output = connection.send_config_from_file(config_file)
    print(output, file=log_file)

    # Disconnect from switch
    print(Fore.YELLOW + f'\nClosing connection to {switch_name} ({switch_ip})' + Style.RESET_ALL)
    connection.disconnect()

    # Break up the connections for easier tracking
    print(Fore.BLUE + '#' * 100 + Style.RESET_ALL)

print(f"\nScript completed. Log file: switch_banners.log")